// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include <random>
#include "DrawDebugHelpers.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.0f;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElements(4);
	SpawnFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement()
{
	auto HorizontalOffset = SnakeElements.Num() * ElementSize;
	FTransform SpawnTransform{ FVector(HorizontalOffset, 0, 0) };

	auto NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, SpawnTransform);

	NewSnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	NewSnakeElement->SnakeOwner = this;
	SnakeElements.Add(NewSnakeElement);
	int32 ElemIndex = SnakeElements.Find(NewSnakeElement);
	if (ElemIndex == 0)
	{
		NewSnakeElement->SetFirstElementType();
	}
}

void ASnakeBase::AddSnakeElements(int Amount)
{
	for (int i = 0; i < Amount; ++i)
	{
		AddSnakeElement();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X = ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X = -ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y = -ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y = ElementSize;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (!IsValid(OverlappedElement))
	{
		return;
	}
	IInteractable* InteractableInterface = Cast<IInteractable>(Other);
	if (!InteractableInterface)
	{
		return;

	}

	int32 ElemIndex;
	SnakeElements.Find(OverlappedElement, ElemIndex);
	bool bIsFirst = ElemIndex == 0;
	InteractableInterface->Interact(this, bIsFirst);
}

FVector ASnakeBase::CalculateFoodLocation()
{
	// C++14-style random number generation
	std::random_device RandomDevice;
	std::default_random_engine RandomEngine(RandomDevice());
	std::uniform_int_distribution<int> UniformDistribution(0, 3);
	int RandomCornerIndex{ UniformDistribution(RandomEngine) };
	std::uniform_real_distribution<float> UniformFloatDistribution(1.5f, 3.5f);
	float RandomLerpAlpha{ UniformFloatDistribution(RandomEngine) };

	TArray<FVector> Corners{
		FVector(-500, -500, 20),
		FVector(+500, -500, 20),
		FVector(-500, +500, 20),
		FVector(+500, +500, 20)
	};
	// choose random corner
	FVector RandomCorner{ Corners[RandomCornerIndex] };

	// raycast to snake head
	FHitResult RaytraceHitResult(ForceInitToZero);
	GetWorld()->LineTraceSingleByObjectType(RaytraceHitResult, RandomCorner, SnakeElements[0]->GetActorLocation(), FCollisionObjectQueryParams::AllObjects);

	// Determine direction to spawn food in
	FVector ClosestPointOnSnake = RaytraceHitResult.Location;

	FVector FoodSpawnDirection = RandomCorner - ClosestPointOnSnake;
	FoodSpawnDirection.Normalize();

	// Scale direction by several lengths of food (I'm using snake element size for reference and simplicity)
	FoodSpawnDirection *= RandomLerpAlpha * ElementSize;

	// Result is the raycast hit on snake head plus food spawn direction scaled randomly
	FVector Result = ClosestPointOnSnake + FoodSpawnDirection;

	DrawDebugLine(GetWorld(), RandomCorner, ClosestPointOnSnake, FColor::Magenta, false, 2.0f);
	DrawDebugDirectionalArrow(GetWorld(), RandomCorner, Result, 60.0f, FColor::Emerald, false, 2.0f);

	return Result;
}

void ASnakeBase::SpawnFood()
{
	UE_LOG(LogTemp, Display, TEXT("Spawning Food"));
	FVector PreparedLocation{ CalculateFoodLocation() };
	UE_LOG(LogTemp, Display, TEXT("Food Location: %f, %f, %f"), PreparedLocation.X, PreparedLocation.Y, PreparedLocation.Z);
	CurrentExistingFood = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform{ PreparedLocation });
}
