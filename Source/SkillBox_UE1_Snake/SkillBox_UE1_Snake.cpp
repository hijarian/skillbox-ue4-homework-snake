// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBox_UE1_Snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillBox_UE1_Snake, "SkillBox_UE1_Snake" );
