// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillBox_UE1_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOX_UE1_SNAKE_API ASkillBox_UE1_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
